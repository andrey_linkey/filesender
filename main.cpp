#include <iostream>
#include <cerrno>
#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <unistd.h>
#include <functional>
#include <thread>
#include <system_error>
#include <vector>
#include <csignal>
#include <memory>

#include "client_handler.h"

using client_handler_ptr = std::unique_ptr<client_handler>;
using client_thread_ptr = std::unique_ptr<std::thread>;

const std::string DIRECTORY = ".";
const unsigned short PORT = 8888;
const unsigned short CLIENTS_MAX = 5;
const int TIMEOUT = 500;
const unsigned long BUFFER_SIZE = 1500;

static std::vector<client_handler_ptr> chvec;
static std::vector<client_thread_ptr> thvec;
static bool terminate = false;
static int client_idx = 0;

int prepare_socket(unsigned short port)
{
    int srv_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(srv_fd < 0)
        throw std::system_error(std::error_code(errno, std::generic_category()),
                                "error while creating socket");

    int enable = 1;
    if(setsockopt(srv_fd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0)
        throw std::system_error(std::error_code(errno, std::generic_category()),
                                "error while setting options for socket");

    struct sockaddr_in address;
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(port);

    if(bind(srv_fd, reinterpret_cast<struct sockaddr*>(&address), sizeof(address)) < 0)
        throw std::system_error(std::error_code(errno, std::generic_category()),
                                "error while binding socket");

    if(listen(srv_fd, CLIENTS_MAX) < 0)
        throw std::system_error(std::error_code(errno, std::generic_category()),
                                "unable to listen socket");
    return srv_fd;
}

void accept_connection(int server_fd, std::string& directory, int timeout)
{
    struct pollfd fds;
    fds.fd = server_fd;
    fds.events = POLLIN;

    int poll_res = poll(&fds, 1, timeout);
    switch (poll_res) {
    case -1:
    case 0:
        return;
    default:
        int client_fd = accept(server_fd, nullptr, nullptr);
        if(client_fd < 0)
            throw std::system_error(std::error_code(errno, std::generic_category()),
                                    "unable to accept connection");

        std::string file_name(directory + "/client_" + std::to_string(++client_idx) + "_output.bin");

        chvec.emplace_back(new client_handler(client_fd, timeout, BUFFER_SIZE, file_name));
        thvec.emplace_back(new std::thread(std::bind(&client_handler::run, chvec.back().get())));
        std::cout << "client number " << std::to_string(client_idx) << " accepted" << std::endl;
    }
}

void cleanup_terminated()
{
    std::vector<client_handler_ptr>::iterator it = chvec.begin();
    while(it < chvec.end())
    {
        if((*it) -> terminated())
        {
            long idx = std::distance(chvec.begin(), it);
            it = chvec.erase(it);
            thvec[static_cast<unsigned long>(idx)] -> join();
            thvec.erase(thvec.begin() + idx);
            continue;
        }
        ++it;
    }
}

void sig_handler(int signal)
{
    terminate = true;
}

void begin_accept(int srv_fd, std::string& directory)
{
    std::signal(SIGINT, sig_handler);
    std::signal(SIGTERM, sig_handler);

    while(!terminate)
    {
        try
        {
            cleanup_terminated();
            accept_connection(srv_fd, directory, TIMEOUT);
        }
        catch (const std::exception &e)
        {
            std::cerr << "error while accepting connection: " << e.what() << std::endl;
            return;
        }

    }
    for(client_handler_ptr& hdl: chvec)
        hdl -> exit();
    for(client_thread_ptr& thr: thvec)
        thr -> join();

    close(srv_fd);
}

int main(int argc, char *argv[])
{
    std::string directory(DIRECTORY);
    unsigned short port = PORT;
    bool demonize = false;

    int opt;
    while((opt = getopt(argc, argv, "p:o:dh")) != -1)
    {
        switch(opt) {
        case 'p':
            try
            {
                port = static_cast<unsigned short>(std::stoi(std::string(optarg)));
            }
            catch (...)
            {
                std::cerr << "invalid value of argument -p: " << optarg << std::endl;
                return 1;
            }
            break;
        case 'o':
            directory = std::string(optarg);
            break;
        case 'd':
            demonize = true;
            break;
        case 'h':
            std::cout << "-p port number\n-o directory to store files\n-d demonize server"
                      << std::endl;
            return 0;
        }
    }

    int srv_fd = 0;

    try
    {
        srv_fd = prepare_socket(port);
    }
    catch (const std::exception &e)
    {
        std::cerr << "error while preparing socket: " << e.what() << std::endl;
        return 1;
    }

    std::cout << "server started\nport is " << std::to_string(port)
              << ", output directory is " << directory
              << "\nyou can change default options using argumets" << std::endl;

    int pid;
    if(demonize)
    {
        pid = fork();
        if (pid == -1)
        {
            std::cerr << "failed to start daemon" << std::endl;
            return 1;
        }
        else if (!pid)
        {
            umask(0);
            chdir("/");
            begin_accept(srv_fd, directory);
        }
    }
    else
        begin_accept(srv_fd, directory);

    return 0;
}
