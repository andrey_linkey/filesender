#include "client_handler.h"

client_handler::client_handler(int client_fd, int timeout, unsigned long buffer_size, std::string file_name)
 : fd_(client_fd), timeout_(timeout), fname_(file_name), data_(buffer_size), terminate_(false)
{

}

long client_handler::read_data_()
{
    struct pollfd fds;
    fds.fd = fd_;
    fds.events = POLLIN;

    while(true)
    {
        int poll_res = poll(&fds, 1, timeout_);

        switch (poll_res) {
        case -1:
            throw std::system_error(std::error_code(errno, std::generic_category()),
                                    "error while poll socket");
        case 0:
            if(terminate_)
                return 0;
            break;

        default:
            long count = read(fd_, &data_[0], data_.size());
            if(count > 0)
                return count;
            else
                std::cout << "socket closed by other side" << std::endl;
                terminate_ = true;
                return 0;
        }
    }
}

void client_handler::run()
{
    std::ofstream client_fs(fname_, std::ios::binary | std::ios::out | std::ios::trunc);
    if((client_fs.rdstate() & std::fstream::failbit) != 0)
    {
        std::cerr << "unable to write file " << fname_ << std::endl;
        terminate_ = true;
    }

    std::cout << "receiving file " << fname_ << " started" << std::endl;
    while(!terminate_)
    {
        long count = 0;
        try
        {
            count = read_data_();
        }
        catch (const std::exception &e)
        {
            terminate_ = true;
            std::cerr << "error while receiving file: " << e.what() << std::endl;
            break;
        }

        if(count > 0)
        {
            if(client_fs.is_open())
                client_fs.write(&data_[0], count);
        }
        else
        {
            if(terminate_)
                break;
        }
    }
    client_fs.close();
    close(fd_);
    std::cout << "file " << fname_ << " transfer ended" << std::endl;
}

bool client_handler::terminated()
{
    return terminate_;
}

void client_handler::exit()
{
    terminate_ = true;
}

