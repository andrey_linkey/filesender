#include <iostream>
#include <fstream>
#include <cerrno>
#include <csignal>
#include <system_error>
#include <vector>
#include <sys/poll.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>

const std::string FILE_NAME("./test_file.bin");
const std::string IPADDR("127.0.0.1");
const unsigned short PORT = 8888;
const int TIMEOUT = 500;
const unsigned long BUFFER_SIZE = 1500;

static bool terminate = false;

int prepare_socket(char* ip_address, unsigned short port)
{
    int clt_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(clt_fd < 0)
        throw std::system_error(std::error_code(errno, std::generic_category()),
                                "error while creating socket");

    struct sockaddr_in address;
    address.sin_family = AF_INET;
    address.sin_port = htons(port);

    if(inet_pton(AF_INET, ip_address, &address.sin_addr) <= 0)
        throw std::system_error(std::error_code(errno, std::generic_category()),
                                "error while converting address");


    if(connect(clt_fd, reinterpret_cast<struct sockaddr*>(&address), sizeof(address)) < 0)
        throw std::system_error(std::error_code(errno, std::generic_category()),
                                "error while connecting to server");
    return clt_fd;
}

unsigned long read_data(std::ifstream& data_fs, char* buff, unsigned long buff_len)
{
    if((data_fs.rdstate() & std::fstream::eofbit) != 0)
        return 0;

    return static_cast<unsigned long>(data_fs.readsome(buff, static_cast<long>(buff_len)));
}

void sig_handler(int signal)
{
    terminate = true;
}

int main(int argc, char *argv[])
{
    std::string file_name(FILE_NAME);
    std::string ip_addr(IPADDR);
    unsigned short port = PORT;

    int opt;
    while((opt = getopt(argc, argv, "a:p:f:h")) != -1)
    {
        switch(opt) {
        case 'a':
            ip_addr = std::string(optarg);
            break;
        case 'p':
            try
            {
                port = static_cast<unsigned short>(std::stoi(std::string(optarg)));
            }
            catch (...)
            {
                std::cerr << "invalid value of argument -p: " << optarg << std::endl;
                return 1;
            }
            break;
        case 'f':
            file_name = std::string(optarg);
            break;
        case 'h':
            std::cout << "-a server ip address\n-p port number\n-f file name" << std::endl;
            break;

        }
    }

    std::signal(SIGINT, sig_handler);
    std::signal(SIGTERM, sig_handler);

    std::ifstream data_fs(file_name.c_str(), std::ios::binary | std::ios::in);
    if((data_fs.rdstate() & std::fstream::failbit) != 0)
    {
        std::cerr << "unable to read file " << file_name << std::endl;
        return -1;
    }

    int clt_fd = 0;
    try
    {
        clt_fd = prepare_socket(const_cast<char*>(ip_addr.c_str()), port);
    }
    catch (const std::exception &e)
    {
        std::cerr << "error while preparing socket: " << e.what() << std::endl;
        return 1;
    }

    std::cout << "sending file " << file_name << " to server " << ip_addr << ":" << port
              << "\nyou can change default options using argumets" << std::endl;
    std::vector<char> raw_data(BUFFER_SIZE);
    while(!terminate)
    {
        struct pollfd fds;
        fds.fd = clt_fd;
        fds.events = POLLRDHUP;

        if(poll(&fds, 1, TIMEOUT) > 0)
        {
            std::cerr << "connection closed by other side" << std::endl;
            break;
        }

        unsigned long readed_count = read_data(data_fs, &raw_data[0], raw_data.size());
        if(readed_count == 0)
        {
            std::cout << "file transfered successfully" << std::endl;
            break;
        }

        long count = write(clt_fd, &raw_data[0], readed_count);
        if(count <= 0)
        {
            std::cerr << "error while writing data: " << std::to_string(errno) << std::endl;
            break;
        }
    }
    close(clt_fd);
    data_fs.close();
    return 0;
}
