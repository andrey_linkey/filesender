#ifndef CLIENT_HANDLER_H
#define CLIENT_HANDLER_H

#include <atomic>
#include <cerrno>
#include <iostream>
#include <fstream>
#include <vector>
#include <sys/poll.h>
#include <unistd.h>
#include <system_error>

class client_handler
{
public:
    client_handler(int client_fd, int timeout, unsigned long buffer_size, std::string file_name);
    void run();
    bool terminated();
    void exit();

private:
    long read_data_();

    int fd_;
    int timeout_;
    std::string fname_;
    std::vector<char> data_;
    std::atomic_bool terminate_;
};

#endif // CLIENT_HANDLER_H
